class GUI {
    constructor(handler) {
        this.handler = handler;
        this.body = document.getElementsByTagName('body')[0];
        this.divs = {
            maindiv: document.createElement('div'),
            select: document.createElement('select'),
            data: document.createElement('div'),
            rows: {
                bots: document.createElement('row'),
                eject: document.createElement('row'),
                split: document.createElement('row'),
                botgamemode: document.createElement('row'),
                botdestination: document.createElement('row'),
                vshield: document.createElement('row'),
                endtime: document.createElement('row'),
                startStop: document.createElement('row'),
            },
            nel: document.createElement('iframe')
        }
        this.inputs = {
            eject: document.createElement('input'),
            split: document.createElement('input'),
            botgamemode: document.createElement('input'),
            botdestination: document.createElement('input'),
            vShield: document.createElement('input'),
            startStop: document.createElement('input')
        }
        this.rowsinit = false;
        this.initialize();
    }

    initialize() {
        try {
            this.divs.maindiv.setAttribute('class', 'muzza-gui');
            this.divs.maindiv.setAttribute('style', 'width: 260px;position: fixed;z-index: 999;zoom:0.8;transform: translate(15px, 30vh);height: 332px;background: rgba(0, 0, 0, 0.7);border: 2px solid grey;padding: 10px 0px 0px 0px;border-radius: 5%;');

            const labels = ["agarbots", "morebots"];
            for (let i = 0; i < labels.length; i++) {
                const option = document.createElement("option");
                option.value = labels[i];
                option.text = labels[i];
                option.style.background = '#000000'
                this.divs.select.appendChild(option);
            }
            this.divs.select.setAttribute('style', 'width:100%;background: rgba(0, 0, 0, 0.7);color:#FFFFFF;font-size: 22px;')
            this.divs.select.setAttribute('id', 'bots-label');
            this.divs.maindiv.appendChild(this.divs.select);

            this.divs.data.setAttribute('class', 'data');
            this.divs.data.setAttribute('style', 'margin-top: 12px');
            this.divs.maindiv.appendChild(this.divs.data);

            for (const obj of Object.keys(this.inputs)) {
                this.inputs[obj].setAttribute('id', obj);
                this.inputs[obj].setAttribute('maxlength', "1");
                this.inputs[obj].setAttribute('style', 'width: 30px;height: 30px;margin-right: 3px;background: #3498db;padding: 0px 10px;border: none;color: #FFF;text-align: center;cursor: pointer;outline: none;border-radius: 50%;float: right;text-transform: uppercase;');
            }

            for (const obj of Object.keys(this.divs.rows)) {
                this.divs.rows[obj].setAttribute('class', 'muzza-row');
                this.divs.rows[obj].setAttribute('style', 'color: #FFF;line-height: 32px;font-size: 15px;font-family: \'Ubuntu\', sans-serif;margin: 0 auto;width: 208px;margin-left: 12px;');
                this.divs.data.appendChild(this.divs.rows[obj]);
                let temp = document.createElement('hr');
                temp.setAttribute('id', obj);
                temp.setAttribute('style', 'visibility: hidden;width: 0%;margin: 0;border-color: green;');
                this.divs.data.appendChild(temp);
            }
            this.divs.nel.name = 'nelbots';
            this.divs.nel.src = 'about: blank';
            this.divs.nel.setAttribute('style', 'background-color:transparent;margin-top: 12px;width:100%;height:200px;');
            //this.divs.nel.style = "transparent";
            this.divs.nel.frameBorder = "0";
            this.divs.nel.allowTransparency = "true";
            this.divs.maindiv.appendChild(this.divs.nel);
            this.body.appendChild(this.divs.maindiv);
            setTimeout(() => {
                this.updateRows();
            }, 1000);
        } catch (err) {
            throw new Error(err);
        }
    }

    updateBar(current, max) {
        const Percentage = ((current / max) * 100) - 1;
        document.getElementById('bots').style.width = `${Percentage}%`;
        document.getElementById('bots').style.visibility = 'visible';
    }

    calculateTime(time) {
        const ActualTime = new Date();
        const EndTime = new Date(time);
        var distance = EndTime - ActualTime;
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        var msg;
        if (distance < 0) msg = 'Expired/No Plan!';
        else msg = `${hours}hrs:${minutes}mins:${seconds}secs`;

        return msg;
    }

    getBotMode() {
        const initialized = this.handler.socket.Transmitter.status.initialized;
        const type = this.handler.socket.Transmitter.status.botmode;
        if (!initialized) return "Not connected!";
        if (type === 0) return "Normal";
        if (type === 1) return "Farmer";
        if (type === 2) return "Normal";
        if (type === 10) return "Farmer";
    }

    getvShieldMode() {
        const initialized = this.handler.socket.Transmitter.status.initialized;
        const type = this.handler.socket.Transmitter.status.vshield;
        if (!initialized) return "Not connected!";
        if (!type) return "Disabled";
        return "Actived";
    }

    getbotdestinationMode() {
        const initialized = this.handler.socket.Transmitter.status.initialized;
        const type = this.handler.socket.Transmitter.status.botdestination;
        if (!initialized) return "Not connected!";
        if (!type) return "Mouse";
        return "Cell";
    }

    onChange(evt) {
        const input = evt.target;
        const val = evt.data;
        if (!val) return this.updateRows();
        if (input === document.getElementById('eject')) {
            this.handler.Hotkeys.keys.eject = val;
            this.handler.Hotkeys.setStorage('eject', val);
        } else if (input === document.getElementById('split')) {
            this.handler.Hotkeys.keys.split = val;
            this.handler.Hotkeys.setStorage('split', val);
        } else if (input === document.getElementById('botgamemode')) {
            this.handler.Hotkeys.keys.botmode = val;
            this.handler.Hotkeys.setStorage('botmode', val);
        } else if (input === document.getElementById('botgamemode')) {
            this.handler.Hotkeys.keys.botdestination = val;
            this.handler.Hotkeys.setStorage('botdestination', val);
        } else if (input === document.getElementById('vShield')) {
            this.handler.Hotkeys.keys.vShield = val;
            this.handler.Hotkeys.setStorage('vShield', val);
        }
    }

    updateRows() {
        if (!this.rowsinit) {
            //document.getElementsByTagName('head')[0].innerHTML += '<style>::selection {background: transparent;}</style>';
            this.divs.rows.eject.innerHTML = `Eject: ${this.handler.GUI.inputs.eject.outerHTML}`;
            this.divs.rows.split.innerHTML = `Split: ${this.handler.GUI.inputs.split.outerHTML}`;
            this.divs.rows.botgamemode.innerHTML = `Bot Mode: <span id="bot_mode_key">${this.getBotMode()}</span> ${this.handler.GUI.inputs.botgamemode.outerHTML}`;
            this.divs.rows.botdestination.innerHTML = `Destination: <span id="botdestination_mode_key">${this.getbotdestinationMode()}</span> ${this.handler.GUI.inputs.botdestination.outerHTML}`;
            this.divs.rows.vshield.innerHTML = `vShield: <span id="vshield_mode_key">${this.getvShieldMode()}</span> ${this.handler.GUI.inputs.vShield.outerHTML}`;
            this.divs.rows.startStop.innerHTML = `Bots Enabled? : <input id="startStop" type="checkbox" ${this.handler.Hotkeys.keys.startStop == '1' ? 'checked' : ''}>`

            document.getElementById('startStop').addEventListener("change", evt => {
                var val = evt.target.checked
                if (val) {
                    this.handler.socket.Transmitter.sendSpawn()
                } else {
                    this.handler.socket.change()
                }
                this.handler.Hotkeys.keys.startStop = val;
                this.handler.Hotkeys.setStorage('startStop', String(Number(val)));
            }, false);
            document.getElementById('eject').value = this.handler.Hotkeys.keys.eject;
            document.getElementById('split').value = this.handler.Hotkeys.keys.split;
            document.getElementById('botgamemode').value = this.handler.Hotkeys.keys.botmode;
            document.getElementById('botdestination').value = this.handler.Hotkeys.keys.botdestination;
            document.getElementById('vShield').value = this.handler.Hotkeys.keys.vShield;

            for (const obj of Object.keys(this.inputs)) {
                this.inputs[obj].setAttribute('id', obj);
                this.inputs[obj].setAttribute('maxlength', "1");
                this.inputs[obj].setAttribute('style', 'width: 30px;height: 30px;margin-right: 3px;background: #3498db;padding: 0px 10px;border: none;color: #FFF;text-align: center;cursor: pointer;outline: none;border-radius: 50%;float: right;text-transform: uppercase;');
                document.getElementById(this.inputs[obj].id).addEventListener("input", evt => this.onChange(evt), false);
                document.getElementById(this.inputs[obj].id).onclick = evt => {
                    evt.target.select()
                };
            }

            this.rowsinit = true;
        } else {
            document.getElementById('eject').value = this.handler.Hotkeys.keys.eject;
            document.getElementById('split').value = this.handler.Hotkeys.keys.split;
            document.getElementById('botgamemode').value = this.handler.Hotkeys.keys.botmode;
            document.getElementById('botdestination').value = this.handler.Hotkeys.keys.botdestination;
            document.getElementById('vShield').value = this.handler.Hotkeys.keys.vShield;

            document.getElementById('bot_mode_key').innerText = this.getBotMode();
            document.getElementById('vshield_mode_key').innerText = this.getvShieldMode();
            document.getElementById('botdestination_mode_key').innerText = this.getbotdestinationMode();
        }
    }

    update(current, max, time) {
        this.updateBar(current, max);
        this.divs.rows.bots.innerHTML = `Bots: ${current}/${max === 500 ? 0 : max}`;
        this.divs.rows.endtime.innerHTML = `End: ${this.calculateTime(time)}`;
    }
}

class Transmitter {
    constructor(socket) {
        this.socket = socket;
        this.status = {
            initialized: false,
            botdestination: 0,
            vshield: 0,
            botmode: 0
        }
    }

    handshake(ver, vext) {
        let buffer = {};
        buffer.action = 17;
        buffer.ver = ver;
        buffer.vext = vext;
        this.socket.send(buffer);
        this.start();
        buffer = {};
        buffer.action = 20;
        this.socket.send(buffer);
    }

    start() {
        this.moveInterval = setInterval(() => {
            this.sendPosition();
        }, 50);
    }

    activePlayer() {
        setTimeout(() => {
            let buffer = {};
            buffer.action = 20;
            this.socket.send(buffer);
        }, 2E3);
    }

    activeClient() {
        let buffer = {};
        buffer.action = 4;
        buffer.leaderBoard = {};
        this.socket.send(buffer);
    }

    sendSpawn() {
        const nick = window.bots.nick,
            ip = window.bots.ws,
            clientKey = window.bots.clientKey
        let buffer = {};
        buffer.action = 1;
        buffer.clientname = nick;
        buffer.targetIp = `${ip}`;
        buffer.ao = clientKey;
        buffer.targetRoom = "#" + window.bots.partyToken;
        this.socket.send(buffer);
        buffer = {};
        buffer.action = 3;
        this.socket.send(buffer);
        this.status.initialized = true;
        this.socket.handler.GUI.initialized = true;
        this.socket.handler.GUI.updateRows();
    }

    sendEject() {
        let buffer = {};
        buffer.action = 15;
        console.log('sent eject!');
        this.socket.send(buffer);
    }

    sendSplit() {
        let buffer = {};
        buffer.action = 16;
        console.log('sent split!');
        this.socket.send(buffer);
    }

    sendPosition() {
        let buffer = {};
        buffer.action = 2;
        if (this.status.botdestination) {
            buffer.positionX = window.bots.playerX; //window.playerX;
            buffer.positionY = window.bots.playerY; //window.playerY;
        }
        else {
            buffer.positionX = window.bots.cursorX; //window.mouseX;
            buffer.positionY = window.bots.cursorY; //window.mouseY;
        }
        this.socket.send(buffer);
    }

    setBotMode() {
        let buffer = {};
        buffer.action = 18;
        buffer.botmode = this.setBotModeCode();
        console.log('sent bot mode!');
        this.socket.send(buffer);
    }
    vShield() {
        let buffer = {};
        buffer.action = 19;
        buffer.botmode2 = this.setvShieldCode();
        console.log('sent vshield!');
        this.socket.send(buffer);
    }
    botdestination() {
        buffer.botmode2 = this.setbotdestination();
        console.log('sent vshield!');
    }
    setvShieldCode() {
        if (this.status.vshield === 0) {
            this.status.vshield = 1;
        } else if (this.status.vshield === 1) {
            this.status.vshield = 0;
        }
        return this.status.vshield;
    }
    setbotdestination() {
        if (this.status.botdestination === 0) {
            this.status.botdestination = 1;
        } else if (this.status.botdestination === 1) {
            this.status.botdestination = 0;
        }
        return this.status.botdestination;
    }

    setBotModeCode() {
        if (this.status.botmode === 0) {
            this.status.botmode = 1;
        } else if (this.status.botmode === 1) {
            this.status.botmode = 2;
        } else if (this.status.botmode === 2) {
            this.status.botmode = 10;
        } else if (this.status.botmode === 10) {
            this.status.botmode = 0;
        }
        return this.status.botmode;
    }
}

class Reader {
    constructor(socket) {
        this.socket = socket;
        this.player = socket.player;
    }

    read(buffer) {
        const opcode = buffer.data;
        if (opcode === 1339) (this.player.isPremium = true, this.player.PremiumType = 2);
        if (opcode === 1338) (this.player.isPremium = true, this.player.PremiumType = 1);
        if (opcode === 1337) this.player.PureFeeder = true;
        if (opcode === 21) {
            if (!this.player.initialized) this.player.initialized = true;
            if (this.player.initialized) this.socket.Transmitter.activePlayer();
        } else {
            this.player.decoded = JSON.parse(opcode);
            this.socket.Transmitter.activeClient();
            this.socket.handler.GUI.update(this.player.decoded.currentBots, this.player.decoded.maxBots, this.player.decoded.expire);
        }
    }
}

class Hotkeys {
    constructor(handler) {
        this.handler = handler;
        this.storagekey = 'agarbotdelta_hotkeys';
        this.keys = {
            eject: this.getStorage('eject'),
            split: this.getStorage('split'),
            botmode: this.getStorage('botmode'),
            botdestination: this.getStorage('botdestination'),
            vShield: this.getStorage('vShield'),
            startStop: this.getStorage('startStop')
        }
        this.active = new Set();
        this.macro = null;
        this.keydown();
        this.keyup();
    }

    keydown() {
        document.body.addEventListener('keydown', evt => {
            const key = evt.keyCode;
            if (!(key === 8 || evt.ctrlKey || evt.shiftKey || evt.altKey)) {
                if (key === this.getKey(this.keys.eject)) {
                    if (this.isActive(this.keys.eject)) return;
                    this.active.add(this.keys.eject);
                    this.macro = setInterval(() => {
                        this.handler.socket.Transmitter.sendEject();
                    }, 75);
                }
                if (key === this.getKey(this.keys.split)) {
                    if (this.isActive(this.keys.split)) return;
                    this.active.add(this.keys.split);
                    this.handler.socket.Transmitter.sendSplit();
                }
                if (key === this.getKey(this.keys.botmode)) {
                    if (this.isActive(this.keys.botmode)) return;
                    this.active.add(this.keys.botmode);
                    this.handler.socket.Transmitter.setBotMode();
                }
                if (key === this.getKey(this.keys.vShield)) {
                    if (this.isActive(this.keys.vShield)) return;
                    this.active.add(this.keys.vShield);
                    this.handler.socket.Transmitter.vShield();
                }
                if (key === this.getKey(this.keys.botdestination)) {
                    if (this.isActive(this.keys.botdestination)) return;
                    this.active.add(this.keys.botdestination);
                    this.handler.socket.Transmitter.botdestination();
                }
            }
        });
        this.ghostInterval = setInterval(() => {
            if (!window.bots) return;
            if (window.bots.gameMode == ":party") return;
            var c = {};
            c["action"] = 14;
            c["a"] = window.bots.ghostX;
            c["b"] = window.bots.ghostY;
            c["a"] === 0 || c["b"] === 0 || this.handler.socket.send(c);
        }, 500)
        //document.getElementById('play').addEventListener('click', () => this.Transmitter.sendSpawn());
        //document.getElementById('create-room').addEventListener('click', () => this.socket.change());
        //document.getElementById('join-room').addEventListener('click', () => this.socket.change());
    }

    keyup() {
        document.body.addEventListener('keyup', evt => {
            const key = evt.keyCode;
            if (!(key === 8 || evt.ctrlKey || evt.shiftKey || evt.altKey)) {
                if (key === this.getKey(this.keys.eject)) (this.active.delete(this.keys.eject), clearInterval(this.macro));
                if (key === this.getKey(this.keys.split)) this.active.delete(this.keys.split);
                if (key === this.getKey(this.keys.botmode)) this.active.delete(this.keys.botmode);
                if (key === this.getKey(this.keys.botdestination)) this.active.delete(this.keys.botdestination);
                if (key === this.getKey(this.keys.vShield)) this.active.delete(this.keys.vShield);
                this.handler.GUI.updateRows();
            }
        })
    }

    setStorage(type, key) {
        const storage = JSON.parse(localStorage.getItem(this.storagekey));
        storage[type] = key;
        localStorage.setItem(this.storagekey, JSON.stringify(storage));
        this.keys[type] = key.toUpperCase();
        this.handler.GUI.updateRows();
    }

    getStorage(key) {
        if (!localStorage.hasOwnProperty(this.storagekey)) localStorage.setItem(this.storagekey, JSON.stringify({
            eject: 'C',
            split: 'X',
            botmode: 'M',
            botdestination: 'D',
            vShield: 'V',
            startStop: '1'
        }));
        return JSON.parse(localStorage.getItem(this.storagekey))[key];
    }

    isActive(key) {
        return this.active.has(key);
    }

    getKey(key) {
        return key.toUpperCase().charCodeAt();
    }
}

class AgarBot {
    constructor(handler) {
        this.handler = handler
        this.ip = '';
        this.ws = null;
        this.player = {
            isPremium: false,
            PremiumType: 0,
            PureFeeder: false,
            startTime: Date.now(),
            decoded: {},
            initialized: false
        }
        this.Reader = new Reader(this);
        this.Transmitter = new Transmitter(this);
        //this.connect(ip);


        this.get = function (url, cb) {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.send();
            xhr.onload = function () {
                if (xhr.status != 200) {
                    alert("Response failed");
                } else {
                    cb(xhr.responseText)
                }
            }
            xhr.onerror = function () {
                alert("Request failed");
            };
        }
        //this.change()

        window.bots.on('spawn', () => {
            if (this.handler.Hotkeys.keys.startStop == '1') this.Transmitter.sendSpawn()
        })
        window.bots.on('estabilished', () => {
            this.change()
        })
    }

    connect(ip) {
        this.ip = ip;
        this.ws = new WebSocket(this.ip);
        this.ws.onopen = () => this.onopen();
        this.ws.onmessage = obj => this.Reader.read(obj);
        this.ws.onerror = () => console.log('[BOT] Error while connecting!');
        this.ws.onclose = () => console.log('[BOT] Closed the connessione!');
    }

    onopen() {
        console.log('[BOT] Authenticating to the server!');
        this.Transmitter.handshake('220720', 'Agarbot_Delta')

    }

    send(data) {
        if (!this.ws || !this.ip || this.ws.readyState !== 1) return;
        this.ws.send(JSON.stringify(data));
    }

    reset() {
        this.player = {
            isPremium: false,
            PremiumType: 0,
            PureFeeder: false,
            startTime: Date.now(),
            decoded: {
                currentBots: 0
            },
            initialized: false
        }
        this.Transmitter.status = {
            initialized: false,
            vshield: 0,
            botmode: 0,
            botdestination: 0
        }
    }

    change() {
        this.ws && this.ws.close();
        this.reset();
        window.bots.ws.includes('agar.io') && this.handler.socket === this && this.connect('wss://gamesrv.agarbot.ovh:8443')
    }
}
class MoreBots {
    constructor(handler) {
        this.handler = handler
        this.ip = '';
        this.ws = null;
        this.player = {
            isPremium: false,
            PremiumType: 0,
            PureFeeder: false,
            startTime: Date.now(),
            decoded: {},
            initialized: false
        }
        this.Reader = new Reader(this);
        this.Transmitter = new Transmitter(this);
        //this.connect(ip);


        this.get = function (url, cb) {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.send();
            xhr.onload = function () {
                if (xhr.status != 200) {
                    alert("Response failed");
                } else {
                    cb(xhr.responseText)
                }
            }
            xhr.onerror = function () {
                alert("Request failed");
            };
        }
        //this.change()

        window.bots.on('spawn', () => {
            if (this.handler.Hotkeys.keys.startStop == '1') this.Transmitter.sendSpawn()
        })
        window.bots.on('estabilished', () => {
            this.change()
        })
    }

    connect(ip) {
        this.ip = ip;
        this.ws = new WebSocket(this.ip);
        this.ws.onopen = () => this.onopen();
        this.ws.onmessage = obj => this.Reader.read(obj);
        this.ws.onerror = () => console.log('[BOT] Error while connecting!');
        this.ws.onclose = () => console.log('[BOT] Closed the connessione!');
    }

    onopen() {
        console.log('[BOT] Authenticating to the server!');
        this.Transmitter.handshake('220720', 'Morebots_Delta')

    }

    send(data) {
        if (!this.ws || !this.ip || this.ws.readyState !== 1) return;
        this.ws.send(JSON.stringify(data));
    }

    reset() {
        this.player = {
            isPremium: false,
            PremiumType: 0,
            PureFeeder: false,
            startTime: Date.now(),
            decoded: {
                currentBots: 0
            },
            initialized: false
        }
        this.Transmitter.status = {
            initialized: false,
            vshield: 0,
            botmode: 0,
            botdestination: 0
        }
    }

    change() {
        this.ws && this.ws.close();
        this.reset();
        window.bots.ws.includes('agar.io') && this.handler.socket === this && this.connect('wss://gamesrv.agarbot.ovh:8443')
    }
}

class Nel {
    constructor() {
        this.worker = {
            ready: false,
            window: null
        }
        this.server = 'wss://slowmo.herokuapp.com'
        setTimeout(() => this.init(), 2000)
    }
    init() {
        window.addEventListener('message', function (event) {
            try {
                let id = event.source.ID//windows.indexOf(event.source)
                if (event.data.type == 'ready_') {
                    this.worker = { ready: true, window: event.source }
                    console.log('[worker id ' + id + ' ready]')
                }
                if (event.data.type == 'processing_') {
                    this.worker.ready = false
                }
                if (event.data.type == 'token_') {
                    console.log(id, 'token:')//,event.data.data)
                    this.worker.ready = true
                    connection.getAndSendToken(event.data.data, id)
                }
                if (event.data.type == 'unload_') {
                    this.worker = { ready: false, window: null }
                }
            } catch (e) { }
        })
        window.addEventListener('beforeunload', () => {
            this.worker.window.close()
        })
        const win = window.open('https://agar.io/generator', 'nelbots')
    }
    addWindow() {
        const win = window.open('https://agar.io/generator', 'nelbots', 'toolbar,scrollbars,resizable,top=500,left=500,width=500,height=200')
        windows.push(win)
    }
}
class Bots {
    constructor() {
        this.agarbot = new AgarBot(this)
        this.morebots = new MoreBots(this)
        //this.nelbots = new Nel(this)
        this.GUI = new GUI(this);
        this.Hotkeys = new Hotkeys(this);
    }
    get socket() {
        const label = this.GUI.divs.select.value
        return label === 'agarbots' ? this.agarbot : this.morebots
    }
}
setTimeout(function(){
    window.botz = new Bots()
}, 10000)
